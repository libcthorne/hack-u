package glassestext;

import java.awt.Font;
import java.io.InputStream;
 



import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
 
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.util.ResourceLoader;
 
public class Main {
 
	/** The fonts to draw to the screen */
	private UnicodeFont font;
	private TrueTypeFont font2;
 
	/** Boolean flag on whether AntiAliasing is enabled or not */
	private boolean antiAlias = true;
 
	// Glasses resolution
	private final int WIDTH = 640;
	private final int HEIGHT = 480;
	
	// Padding
	private final int TEXT_PADDING_X = 10;
	private final int TEXT_PADDING_Y = 10;
	private final int TEXT_SIZE = 16;
	
	// Text to render
	String displayText = "DEFAULT TEXT";
	
	/**
	 * Start the test 
	 */
	public void start() {
		initGL(WIDTH, HEIGHT);
		init();
 
		while (true) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
			render();
 
			Display.update();
			Display.sync(100);
 
			if (Display.isCloseRequested()) {
				Display.destroy();
				System.exit(0);
			}
		}
	}
 
	/**
	 * Initialise the GL display
	 * 
	 * @param width The width of the display
	 * @param height The height of the display
	 */
	private void initGL(int width, int height) {
		try {
			DisplayMode displayMode = null;
			DisplayMode[] modes = Display.getAvailableDisplayModes();
			
			 for (int i = 0; i < modes.length; i++)
			 {
			     if (modes[i].getWidth() == width
			     && modes[i].getHeight() == height
			     && modes[i].isFullscreenCapable())
			       {
			            displayMode = modes[i];
			       }
			 }
		         
			Display.setDisplayMode(displayMode);
			Display.setFullscreen(true);
			Display.create();
			Display.setVSyncEnabled(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
 
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_SMOOTH);        
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_LIGHTING);                    
 
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);                
        GL11.glClearDepth(1);                                       
 
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
 
        GL11.glViewport(0,0,width,height);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
 
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, width, height, 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}
 
	/**
	 * Initialise resources
	 */
	public void init() {
		// load a default java font
		Font awtFont = new Font("Arial Unicode MS", Font.BOLD, TEXT_SIZE);
		font = new UnicodeFont(awtFont);
		font.getEffects().add(new ColorEffect(java.awt.Color.white));

		font.addAsciiGlyphs();
		font.addGlyphs(0x2e80, 0x2fdf); // CKJ radicals supplement + Kangxi
		font.addGlyphs(0x3040, 0x30ff); // Hiragana + katakana
		font.addGlyphs(0x3300, 0x33ff); // CJK Unified Ideographs Extension A
		font.addGlyphs(0xf900, 0xfaff); //CJK compatibility Ideographs;
	
		try {
			font.loadGlyphs();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		/*// load font from file
		try {
			InputStream inputStream	= ResourceLoader.getResourceAsStream("myfont.ttf");
 
			Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont2 = awtFont2.deriveFont(24f); // set font size
			font2 = new TrueTypeFont(awtFont2, antiAlias);
 
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		displayText = prepareString("こんにちは。Hello! 太陽が燃えている");
	}

	private String prepareString(String str) {
		font.addGlyphs(str);
		
		try {
			font.loadGlyphs();
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		return str;
	}
	
	/**
	 * Game loop render
	 */
	public void render() {
	    // set the color of the quad (R,G,B,A)
	    GL11.glColor3f(0.3f, 0.3f, 0.3f);
 
	    GL11.glDisable(GL11.GL_TEXTURE_2D);
	    
	    // draw quad
	    GL11.glBegin(GL11.GL_QUADS);
	    GL11.glVertex2f(10, HEIGHT-100);
		GL11.glVertex2f(WIDTH-10, HEIGHT-100);
		GL11.glVertex2f(WIDTH-10, HEIGHT-10);
		GL11.glVertex2f(10, HEIGHT-10);
	    GL11.glEnd();
	    
	    GL11.glEnable(GL11.GL_TEXTURE_2D);
	    
		Color.white.bind();
 
		font.drawString(10+TEXT_PADDING_X, HEIGHT-100+TEXT_PADDING_Y, displayText, Color.white);
		//font2.drawString(100, 100, "NICE LOOKING FONTS!", Color.green);
	}
 
	/**
	 * Main method
	 */
	public static void main(String[] argv) {
		Main main = new Main();
		main.start();
	}
}