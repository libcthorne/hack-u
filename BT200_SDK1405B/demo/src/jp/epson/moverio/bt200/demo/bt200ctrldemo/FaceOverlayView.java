package jp.epson.moverio.bt200.demo.bt200ctrldemo;

import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.hardware.Camera.Face;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class FaceOverlayView extends View {
	Paint paint = new Paint();

    private final int VIEW_WIDTH = 960;
    private final int VIEW_HEIGHT = 492;
    
    public RectF faceRect = new RectF();
    private RectF drawRect = new RectF();
    
    private Matrix faceTransformationMatrix;
    private boolean pendingFaceRectUpdate = false;
    
    private final float SAME_FACE_THRESHOLD_DISTANCE = Float.MAX_VALUE;
    
    private CommentDisplay commentDisplay;
    
    private boolean inAnimation = false;

	private int killCounter = 0;
    
	private Bitmap santaBitmap;
	private Bitmap mosaicBitmap;
	
	public FaceOverlayView coupleFace;
	
    public FaceOverlayView(Context context, Rect faceRect) {
        super(context);
        
        Resources res = getResources();
        santaBitmap = BitmapFactory.decodeResource(res, R.drawable.santa);
        
        this.commentDisplay = ((BT200CtrlDemoActivity)context).commentDisplay;
        
		this.faceTransformationMatrix = new Matrix();
        this.faceTransformationMatrix.setScale(1, 1);
        this.faceTransformationMatrix.postScale(VIEW_WIDTH / 2000f, VIEW_HEIGHT / 2000f);
        this.faceTransformationMatrix.postTranslate(VIEW_WIDTH / 2f, VIEW_HEIGHT / 2f);	        	

        updateFaceRect(faceRect);
    }

    private void updateDrawFaceRect() {
    	int faceWidth = (int)(this.faceRect.right - this.faceRect.left);
    	int faceHeight = (int)(this.faceRect.bottom - this.faceRect.top);
    	
        // Set view position
        this.setX(this.faceRect.left-faceWidth/2-100);
        this.setY(this.faceRect.top-faceHeight/2-100);
        
        // Set Rect size
        this.drawRect.left = 0;
        this.drawRect.top = 0;
        this.drawRect.right = faceWidth*2;
        this.drawRect.bottom = faceHeight*2;

    	invalidate();
    }

    public void updateFaceRect(Rect faceRect) {
    	//if (inAnimation)
    	//	return;
    	
	    //if (this.faceRect == null) { // no animation
	    	this.faceRect.set(faceRect);

	    	// Transform camera coordinates ((-1000,1000) etc.) to screen coordinates (top left (0,0))
	        this.faceTransformationMatrix.mapRect(this.faceRect);
	    	
	    	updateDrawFaceRect();
	    /*} else { // animation
	    	inAnimation = true;
	    	
	    	final RectF newFaceRect = new RectF();
	    	newFaceRect.set(faceRect);
	    	this.faceTransformationMatrix.mapRect(newFaceRect);

	    	float previousWidth = this.faceRect.right - this.faceRect.left;
	    	float previousHeight = this.faceRect.bottom - this.faceRect.top;
	    	
	    	final float newWidth = newFaceRect.right - newFaceRect.left;
	    	final float newHeight = newFaceRect.bottom - newFaceRect.top;
	    	
	    	final float xDelta = newFaceRect.centerX() - this.faceRect.centerX();
	    	final float yDelta = newFaceRect.centerY() - this.faceRect.centerY();
	    	
	    	AnimationSet set = new AnimationSet(true);
	    	
	    	TranslateAnimation translate = new TranslateAnimation(0, xDelta, 0, yDelta);
	    	set.addAnimation(translate);
	    	
	    	//ScaleAnimation scale = new ScaleAnimation(1.0f, newWidth/previousWidth, 1.0f, newHeight/previousHeight);
	    	//set.addAnimation(scale);
	    	
	    	set.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					updateDrawFaceRect();
					
					inAnimation = false;
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					
				}
	    		
	    	});
	    	
	    	set.setDuration(3000);
	    	this.startAnimation(set);
	    	
	    	// Update face data
			this.faceRect.set(newFaceRect);
	    }*/
    }
    
    @Override
    public void onDraw(Canvas canvas) {
    	paint.setColor(Color.BLACK);
	    //paint.setStrokeWidth(3);

    	if (this.coupleFace == null) {
    		//canvas.drawRect(drawRect, paint);
    	} else {
    		/*Random r = new Random();
    		
    		for (int i = 0; i < 5; i++) {
    			for (int j = 0; j < 15; j++) {
    				RectF mosaicRect = new RectF(drawRect);
    				
    				int xShift = ((i+1)*5) * r.nextInt(10);
    				mosaicRect.left += xShift;
    				mosaicRect.right += xShift;
    				
    				int yShift = ((j+1)*10) * r.nextInt(5);
    				mosaicRect.top += yShift;
    				mosaicRect.bottom += yShift;
    				
    				canvas.drawBitmap(mosaicBitmap, null, mosaicRect, paint);
    			}
    		}*/
    		
    		RectF santaRect = new RectF(drawRect);
    		santaRect.bottom = santaRect.top + 600;
    		
    		santaRect.right = santaRect.left + 450;
    		santaRect.left -= 0;
    		
    		canvas.drawBitmap(santaBitmap, null, santaRect, paint);
    	}
    	
    	//invalidate();
    }

	public static float distance(FaceOverlayView view1, FaceOverlayView view2) {
		return Math.abs(view1.faceRect.centerX() - view2.faceRect.centerY());
	}

	public Face getNearestFaceToSelf(List<Face> faces) {
		Face nearestFace = null;
		float smallestDistance = Float.MAX_VALUE;
		
		PointF selfFaceCenter = new PointF(this.faceRect.centerX(), this.faceRect.centerY());
		float selfFaceCenterLen = PointF.length(selfFaceCenter.x, selfFaceCenter.y);
		
		for (Face face : faces) {
			PointF targetFaceCenter = new PointF(face.rect.exactCenterX(), face.rect.exactCenterY());
			
			float targetFaceCenterLen = PointF.length(targetFaceCenter.x, targetFaceCenter.y);
			
			float distance = Math.abs(selfFaceCenterLen - targetFaceCenterLen);
			
			if (distance < smallestDistance && distance < SAME_FACE_THRESHOLD_DISTANCE) {
				smallestDistance = distance;
				nearestFace = face;
			}
		}
	
		return nearestFace;
	}

	public FaceOverlayView getNearestSingleFaceOverlayViewToSelf(List<FaceOverlayView> facesOnScreen) {
		FaceOverlayView nearestFaceOverlayView = null;
		float smallestDistance = Float.MAX_VALUE;
		
		float selfX = this.faceRect.centerX();
		
		for (FaceOverlayView faceOverlayView : facesOnScreen) {
			if (this == faceOverlayView)
				continue;
			
			// Ignore faces already in a couple
			if (faceOverlayView.coupleFace != null)
				continue;
			
			float targetX = faceOverlayView.faceRect.centerX();
			
			float distance = Math.abs(selfX - targetX);
			
			if (distance < smallestDistance) {
				smallestDistance = distance;
				nearestFaceOverlayView = faceOverlayView;
			}
		}
		
		return nearestFaceOverlayView;
	}
}
