package jp.epson.moverio.bt200.demo.bt200ctrldemo;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.hardware.Camera.Face;
import android.view.View;

public class DrawSquare extends View {
    Paint paint = new Paint();

    private Face[] faces;
    
    private Matrix faceTransformationMatrix = new Matrix();
    private RectF faceRectangle = new RectF();
    
    public DrawSquare(Context context) {
        super(context);
    }

    public void setFaces(Face[] faces) {
    	this.faces = faces;
    	invalidate();
    }
    
    @Override
    public void onDraw(Canvas canvas) {
        if (faces == null)
        	return;
        
    	for (Face face : faces) {
    		faceRectangle.set(face.rect);
    		
    		faceTransformationMatrix.setScale(2, 2);
    		faceTransformationMatrix.postScale(getWidth() / 2000f, getHeight() / 2000f);
    		faceTransformationMatrix.postTranslate(getWidth() / 2f, getHeight() / 2f);
    		
    		faceTransformationMatrix.mapRect(faceRectangle);
            
        	paint.setColor(Color.CYAN);
	        paint.setStrokeWidth(3);
	        
	        canvas.drawRect(faceRectangle, paint);
        }
    }

}
