package jp.epson.moverio.bt200.demo.bt200ctrldemo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class BT200CtrlDemoActivity extends Activity {
	private String TAG = "Bt2CtrlDemoActivity";

	private RelativeLayout applicationLayout;
	public CommentDisplay commentDisplay;
	
	private Handler handler;

	public void addToAppLayout(View view) {
		applicationLayout.addView(view);
	}
	
	public void removeFromAppView(View view) {
		applicationLayout.removeView(view);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		applicationLayout = new RelativeLayout(this);
		
		Window win = getWindow();

		// Set full screen
		//WindowManager.LayoutParams winParams = win.getAttributes();
		//winParams.flags |= 0x80000000;
		//win.setAttributes(winParams);
		
		// Hide title bar
		win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		
		// Set background to black
		//applicationLayout.setBackgroundColor(Color.parseColor("#000000"));
		
		// Handler for adding UI elements outside of UI thread
		handler = new Handler();
		
		// Comment display
		commentDisplay = new CommentDisplay(this, handler, applicationLayout);
		
		// Test comments
		//commentDisplay.test();

		// Face drawer
		DrawSquare drawSquare = new DrawSquare(this);
		applicationLayout.addView(drawSquare);
		
		// Camera
		SurfaceView surfaceView = new SurfaceView(this);
		SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(new CameraHolderCallback(this, drawSquare, commentDisplay));
        
        // Add camera preview view
		setContentView(surfaceView);
		
		// Add overlay
		addContentView(applicationLayout, new LayoutParams(2000, 2000));

		/*
		// Draw test square
		DrawSquare drawSquare = new DrawSquare(this);
		drawSquare.setX(100);
		drawSquare.setY(100);
		
		Animation squareAnimation = new TranslateAnimation(0.0f, 50.0f, 0.0f, 50.0f);
		squareAnimation.setRepeatCount(Animation.INFINITE);
		squareAnimation.setDuration(1000);
		
		drawSquare.startAnimation(squareAnimation);
		
		applicationLayout.addView(drawSquare);
		*/
	}
}
