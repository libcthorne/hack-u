package jp.epson.moverio.bt200.demo.bt200ctrldemo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;

public class CameraHolderCallback implements Callback {

	protected static final float COUPLE_DISTANCE = 700;

	private Camera camera;

	private Activity activity;
	private CommentDisplay commentDisplay;

	private List<FaceOverlayView> facesOnScreen = new ArrayList<FaceOverlayView>();

	public CameraHolderCallback(Activity activity, DrawSquare drawSquare,
			CommentDisplay commentDisplay) {
		this.activity = activity;
		this.commentDisplay = commentDisplay;
	}

	private FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {
		@Override
		public void onFaceDetection(Face[] _faces, Camera camera) {
			// drawSquare.setFaces(faces);

			List<Face> facesList = new ArrayList<Face>(Arrays.asList(_faces));

			Iterator<FaceOverlayView> facesOnScreenI = facesOnScreen.iterator();

			// Update existing face views
			while (facesOnScreenI.hasNext()) {
				FaceOverlayView faceOverlayView = facesOnScreenI.next();

				// Find nearest face
				Face nearestFace = faceOverlayView.getNearestFaceToSelf(facesList);

				if (nearestFace == null) {
					// Unregister self as couple
					if (faceOverlayView.coupleFace != null) {
						faceOverlayView.coupleFace.coupleFace = null;
						faceOverlayView.coupleFace = null;
						
						//commentDisplay.createComment("カップル消えた (´･ω･`)");
					}
					
					facesOnScreenI.remove();
					((BT200CtrlDemoActivity) activity).removeFromAppView(faceOverlayView);
				
					continue;
				}

				// If face is found, remove it from list of faces to examine
				facesList.remove(nearestFace);

				// Update view position
				faceOverlayView.updateFaceRect(nearestFace.rect);
			}

			// Add new face views
			for (Face face : facesList) {
				// Create new view
				FaceOverlayView faceOverlayView = new FaceOverlayView(activity, face.rect);
		        
				((BT200CtrlDemoActivity)activity).addToAppLayout(faceOverlayView);

				// Register view
				facesOnScreen.add(faceOverlayView);
			}
			
			// Check existing couples
			for (FaceOverlayView faceOverlayView : facesOnScreen) {
				FaceOverlayView coupleFace = faceOverlayView.coupleFace;
				
				if (coupleFace == null)
					continue;
				
				float distanceScale = Math.min(1.0f,(faceOverlayView.faceRect.height()*4)/492.0f);

				if (FaceOverlayView.distance(faceOverlayView, coupleFace) > COUPLE_DISTANCE*distanceScale) {
					faceOverlayView.coupleFace.coupleFace = null;
					faceOverlayView.coupleFace = null;
					
					//commentDisplay.createComment("カップル離れた (´･ω･`)");
				}
			}
			
			// Detect new couples
			for (FaceOverlayView faceOverlayView : facesOnScreen) {
				// Already in a couple
				if (faceOverlayView.coupleFace != null)
					continue;
				
				// Find nearest face
				FaceOverlayView nearestSingleFaceOverlayView = faceOverlayView.getNearestSingleFaceOverlayViewToSelf(facesOnScreen);
				
				// No face: continue
				if (nearestSingleFaceOverlayView == null)
					continue;
				
				float distance = Math.abs(faceOverlayView.faceRect.centerX()-nearestSingleFaceOverlayView.faceRect.centerX());
				float distanceScale = Math.min(1.0f,(faceOverlayView.faceRect.height()*4)/492.0f);
				
				
				if (distance <= COUPLE_DISTANCE*distanceScale) {
					faceOverlayView.coupleFace = nearestSingleFaceOverlayView;
					nearestSingleFaceOverlayView.coupleFace = faceOverlayView;
					
					//commentDisplay.createComment("カップル発見 (´･ω･`)");
					//
				}
				
				//commentDisplay.setDebugComment("Distance: " + distance + "\n" + "Scale: " + distanceScale);
			}
		}
	};

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		camera = Camera.open();

		Camera.Parameters parameters = camera.getParameters();
		parameters.setZoom(4);
		camera.setParameters(parameters);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		camera.stopPreview();
		camera.release();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		try {
			camera.setPreviewDisplay(holder);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Start preview
		camera.startPreview();

		// Start face detection
		camera.setFaceDetectionListener(faceDetectionListener);
		camera.startFaceDetection();
	}

}
