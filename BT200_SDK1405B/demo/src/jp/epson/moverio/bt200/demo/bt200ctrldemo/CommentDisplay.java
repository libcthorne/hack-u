package jp.epson.moverio.bt200.demo.bt200ctrldemo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.view.Display;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CommentDisplay {
	private Activity activity;
	private Handler handler;
	private RelativeLayout applicationLayout;

	private List<TextView> commentTextViewList = new ArrayList<TextView>();
	private TextView debugTextView;
	
	private final Set<Integer> occupiedScreenHeights = new HashSet<Integer>();
	
	public CommentDisplay(Activity activity, Handler handler, RelativeLayout applicationLayout) {
		this.activity = activity;
		this.handler = handler;
		this.applicationLayout = applicationLayout;
		
		debugTextView = new TextView(activity);
		debugTextView.setText("");
		debugTextView.setTextSize(35.0f);
		debugTextView.setTextColor(Color.parseColor("#FFFFFF"));
		debugTextView.setX(200);
		debugTextView.setY(400);
		applicationLayout.addView(debugTextView);
	}
	
	public void setDebugComment(final String s) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				debugTextView.setText(s);
			}
		});
	}

	@SuppressLint("NewApi")
	public void createComment(final String s) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				// Display size
				Display display = activity.getWindowManager().getDefaultDisplay();
				final Point size = new Point();
				display.getSize(size); // suppress NewApi
				
				final TextView commentTextView = new TextView(activity);

				commentTextView.setText(s);
				commentTextView.setTextSize(35.0f);
				commentTextView.setTextColor(Color.parseColor("#FFFFFF"));
				commentTextView.setX(0.0f);
				commentTextView.setWidth(2000); // HACK
				
				// TODO:
				int height = 10;
				while (occupiedScreenHeights.contains(height)) {
					height += 40;
				}
				occupiedScreenHeights.add(height);
				commentTextView.setY(height);
				commentTextView.setId(height); // HACK: use height as ID

				// ---
				//activity.addContentView(commentTextView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				applicationLayout.addView(commentTextView);
				
				commentTextViewList.add(commentTextView);

				final Animation commentAnimation = new TranslateAnimation(-2
						* commentTextView.getWidth() - 300,
						2 * size.x + 2 * commentTextView.getWidth(),
						0.0f, 0.0f);
				commentAnimation.setDuration(6000);
				// commentAnimation.setRepeatCount(Animation.INFINITE);

				// Random r = new Random();
				// int delay = r.nextInt(5000);
				// commentAnimation.setStartOffset(delay);

				commentAnimation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
						// ---
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO remove commentTextView after animation finishes
						removeComment(commentTextView);
					}
				});
				commentTextView.startAnimation(commentAnimation);
			}
		});
	}

	public void removeComment(final TextView commentTextView) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				// debugTextView.setText(commentTextView.getText()); // (test)
				commentTextViewList.remove(commentTextView);
				applicationLayout.removeView(commentTextView);
				occupiedScreenHeights.remove(commentTextView.getId()); // HACK
			}
		});
	}

	public void test() {

		// (testing) create comments
		new Thread(new Runnable() {
			public void run() {
				for (int i = 0;; ++i) {
					setDebugComment("created: " + i);
					
					try {
						Thread.sleep(1000);
						createComment("comment " + i);
					} // 1 second pause
					catch (Exception e) {
					}
				}
			}
		}).start();
		createComment("���A�[��������!");

		createComment("a");		
	}

}
